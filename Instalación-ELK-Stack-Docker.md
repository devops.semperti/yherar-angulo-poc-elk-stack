## Detalle


Para esta instalación usaremos tres (3) instancias, cada instancia tendra corriendo un contenedor docker con una herramienta determinada que en conjunto forman ELK Stack.

NOTA: Debido a que para esta instalación vamos a usar como SO RHEL 7, es necesario que se registren los nodos para poder instalar los paquetes necesarios. [Más Información](https://access.redhat.com/es/solutions/770843)


![Config1](Images/images1.png)


### Información de Instancias:


##### 1.- Elasticsearch

    - IP: 172.18.2.171
    - RAM: 4 GB
    - CPU: 2
    - SO: Rhel 7

##### 2.- Logstash:

    - IP: 72.18.2.172
    - RAM: 4 GB
    - CPU: 2
    - SO: Rhel 7

##### 3.- Kibana:

    - IP: 72.18.2.173
    - RAM: 4 GB
    - CPU: 2
    - SO: Rhel 7
    

### Instalación Java RHEL 7


En cada instancia se debe instalar java-1.8.0-openjdk-devel para corregir las advertencias.


##### 1.- Instalacion:


    sudo yum install java-1.8.0-openjdk-devel
    
    
### Instalación de Docker CENTOS 7 / RHEL 7


##### 1.- Desinstalar versiones antiguas de Docker:

        sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine


##### 2.- Instalar usando el repositorio:

        sudo yum install -y yum-utils
        
        sudo yum-config-manager \
            --add-repo \
             https://download.docker.com/linux/centos/docker-ce.repo
             
             
##### 3.- Instalar paquete Docker:


        sudo yum install docker-ce docker-ce-cli containerd.io


### Configuración de Docker:


Se modificó la ruta de montaje de docker en cada instancia, cambiando el path que viene por defecto. Con esto podemos lograr que las imagenes, volumenes y datos de Docker sean alojados en la ruta que le asignemos.


##### 1.- Se detiene el servicio de docker, se hace una copia de seguridad de los archivos de docker y se crea el directorio en la nueva ruta:


            systemctl stop docker            
            
            cd /var/lib
            mv docker docker-backup
            mkdir /opt/docker
            
            
##### 2.- Se establece la nueva ruta y configuramos en deamon de docker:


- 2.1.-  Configuramos el archivo de docker, modificando el parametro *ExecStart* con la nueva ruta. 


            vim /lib/systemd/system/docker.service
            
            ### Original 
            ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
            
            ### Nueva 
            ExecStart=/usr/bin/dockerd -g /opt/docker -H fd:// $DOCKER_OPTS
            
           
- 2.2.- Se crea y se configura el daemon de docker.


            vim /etc/docker/daemon.json
            
                 { 
                    "data-root": "/opt/docker" 
                 }


- 2.3.- Se inicia el servicio y guardamos las configuraciones del daemon de docker.

            
             systemctl daemon-reload
             systemctl start docker
             
             
             
### Volumenes en Docker


Para que la configuración de los contenedores quede segura se creará en cada instancia un volumen en docker (*elasticsearch_data, kibana_data, logstash_data*), configurando la ruta de los archivos de configuración de cada herramienta:


##### 1.- Creacion de los volumenes: 


- Elasticsearch:


             docker volume create elasticsearch_data


- Logstash:


             docker volume create logstash_data
    

- Kibana:


             docker volume create kibana_data


##### 2.- Ruta del directorio que se va a persistir:


1. Elasticsearch: /usr/share/elasticsearch
2. Logstash: /usr/share/logstash
3. Kibana: /usr/share/kibana


### Exporner puertos:


Abriremos los puertos por defecto que usan las herramientas del ELK Stack.


1. **ElasticSearch:** 9200, 9300


            firewall-cmd --zone=public --add-port=9200/tcp --permanent 
 
 
            firewall-cmd --zone=public --add-port=9300/tcp --permanent 


2. **Logstash:** 5044
 

            firewall-cmd --zone=public --add-port=5044/tcp --permanent


3. **Kibana:** 5601
            

            firewall-cmd --zone=public --add-port=5601/tcp --permanent
            
            
            firewall-cmd --reload
            
            
### Docker Run:


##### 1.- Imagenes de las herramientas:


Usaremos la version *6.8.16* de cada herramienta. Descargaremos las imagenes desde docker hub:


    docker pull elasticsearch:6.8.16


    docker pull logstash:6.8.16


    docker pull kibana:6.8.16


Correremos cada contenedor en segundo plano *(-d)*, usando el volumen persistente anteriormente creado *(-v mis_datos)*, especificamos que use la red de nuestro host *( --network host)*, y e enviaremos el parametro --restart unless-stopped *$(docker ps -q)* para que reinicie automaticamente salvo que se haya detenido el contenedor.


**ElasticSearch:**

    docker run -d --name elasticsearch --restart unless-stopped $(docker ps -q) -v  elasticsearch_data:/usr/share/elasticsearch ----network host -e "discovery.type=single-node" elasticsearch:6.8.1

**Logstash:**

    docker run  -d --restart unless-stopped $(docker ps -q) -v logstash_data:/usr/share/logstash --name logstash --network host logstash:6.8.16

 **Kibana:**

    docker run -d -p5601:5601 -h kibana -v kibana_data:/usr/share/kibana --name kibana --network host kibana:6.8.16
  

### Archivos de configuración


Una vez que esten los contenedores corriendo, vamos a modificar los siguientes archivos:


**Logstash:** En el *Logstash.yml* se debe agregar parametro de monitoreo y la ip del elasticsearch. Nota: tambien editar el logstash.conf.


        http.host: "0.0.0.0"
        xpack.monitoring.enabled: true
     xpack.monitoring.elasticsearch.hosts: [ "http://172.18.2.171:9200" ]


**Kibana:** En el *kibana.yml* se debe agregar parametro de monitoreo y la ip del elasticsearch.


    server.name: kibana
    server.host: "0"
    elasticsearch.hosts: [ "http://172.18.2.171:9200" ]
    xpack.monitoring.ui.container.elasticsearch.enabled: true


Una vez culminado los pasos tendremos las herramientas del ELK Stack funcionando y listo para su uso.


**ElasticSearch:**


            curl 172.18.2.171:9200                                                                                                        
        {
            "name" : "m2Zy94k",
            "cluster_name" : "docker-cluster",
            "cluster_uuid" : "uLjU9EcrRzmHw3uW5rMTmQ",
        "version" : {
            "number" : "6.8.16",
            "build_flavor" : "default",
            "build_type" : "docker",
            "build_hash" : "1f62092",
            "build_date" : "2021-05-21T19:27:57.985321Z",
            "build_snapshot" : false,
            "lucene_version" : "7.7.3",
            "minimum_wire_compatibility_version" : "5.6.0",
            "minimum_index_compatibility_version" : "5.0.0"
        },
            "tagline" : "You Know, for Search"
    }


**Kibana:** http://172.18.2.173:5601/












 



        

     



