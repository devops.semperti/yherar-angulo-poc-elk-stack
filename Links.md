### Documentación


1.- ¿Qué es ELK Stack?
* https://www.elastic.co/es/what-is/elasticsearch

2.- Elasticsearch
* https://www.elastic.co/es/what-is/elasticsearch

3.- Kibana
* https://www.elastic.co/es/what-is/kibana

4.- Logstash
* https://www.elastic.co/es/logstash
